import { Pipe, PipeTransform } from '@angular/core';
import { Task } from 'src/app/Model/Task';
import { forEach } from '@angular/router/src/utils/collection';


 
@Pipe({
  name: 'searchPipe' ,pure:false
})
export class SearchPipePipe implements PipeTransform {      

//  transform(value: any, txtTask?: any, txtPrioriyFrom?: any,txtPrioriyTo?:any,dtStartDate?:any,dtEndDate?:any): any { 
  transform(taskList: any, txtTask: any, taskParent:any, txtPrioriyFrom:any, txtPrioriyTo:any, dtStartDate:any, dtEndDate:any): any {


    let filteredList: any=[];


    if (taskList && taskList.length){
      return taskList.filter(item =>{
          if (txtTask && item.TaskName.toLowerCase().indexOf(txtTask.toLowerCase()) === -1){
              return false;
          }
          if (taskParent && item.Parent_ID.toLowerCase().indexOf(taskParent.toLowerCase()) === -1){
              return false;
          }
          if (dtStartDate && item.StartDate.toLocaleString().indexOf(dtStartDate.toLocaleString()) === -1){
              return false;
          }
          if (dtEndDate && item.EndDate.toLocaleString().indexOf(dtEndDate.toLocaleString()) === -1){
            return false;
        }
          return true;
     })
  }
  else{
    return taskList;
}
   
    // if ((!txtTask) && (!txtPrioriyFrom)&& (!txtPrioriyTo)&& (!dtStartDate)&& (!dtEndDate))
    // {
     // alert("entred");
      
     // alert("first");
      //return value;
    //  }
    //  else 
    //  {
     // alert("else")
     // return (value.filter(a=> {return   a.Priority == txtPrioriyFrom || a.TaskName.toLowerCase().includes(txtTask) }));

    //   if(priority){
    //     //return value.filter(a=> { return a.Priority.toLowerCase().includes(priority)});
    //     //filteredList.push(value.filter(a=> {  a.Priority.toLowerCase().includes(priority)}));
    //     alert(2);
    //     alert(priority);
    //     return (value.filter(a=> {return   a.Priority == priority}));
  
    //    }
  
    // if(taskName){
    //   alert(3);
    //  return  value.filter(a=> {return  a.TaskName.toLowerCase().includes(taskName)});
    //  // alert(filteredList);
    // }
    
    //return filteredList;
    //}
  
  }

}
