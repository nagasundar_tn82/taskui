import { TestBed,inject } from '@angular/core/testing';

import { AddTaskService } from './add-task.service';
import { HttpClient } from '@angular/common/http';
import { Task } from 'src/app/Model/Task';
import { Observable } from 'rxjs';

import {HttpClientModule} from '@angular/common/http';

describe('AddTaskService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports:[HttpClientModule],
    providers: [
       AddTaskService ]

  }));

  it('should be created', inject([AddTaskService], (service: AddTaskService) => {
    expect(service).toBeTruthy();
  }));

  it('GetAllTaskDetails() created fpr Service task', inject([AddTaskService], (service: AddTaskService) => {
    //  service.getTasks().subscribe(value=> {expect(value.length).toBeGreaterThanOrEqual(1)});
    expect(service.GetAllTaskDetails()).not.toBeNull();
  }));

  // it('should be created', () => {
  //   let service: AddTaskService;
  //   service = TestBed.get(AddTaskService);
  //   expect(service).toBeTruthy();
  // });
});



 