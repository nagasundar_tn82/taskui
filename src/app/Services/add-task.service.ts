import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Task } from 'src/app/Model/Task';
import { Observable } from 'rxjs';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class AddTaskService {
  

  constructor(private varHttp:HttpClient) { }


  getTask() : Observable<Task>
   { 
     return this.varHttp.get<Task>("http://localhost/WeApiNew/api/Add/GetTaskID");
  }
  getTaskName(value:any) : Observable<Task>
  {     
    return this.varHttp.get<Task>("http://localhost/WeApiNew/api/Add/GetSearchTaskID?TaskID="+ value);
 }

 


 AddTaskDetails(value:Task)  
 {
  return this.varHttp.post<Task>("http://localhost/WeApiNew/api/Add/AddTaskDetails" , value, httpOptions);
 }
 ModifyTaskDetails(value:Task)
 {
  return this.varHttp.put<Task>("http://localhost/WeApiNew/api/Add/UpdateTaskDetails" , value, httpOptions);

 }

 DeleteTaskDetails(value:any)
 {
 return this.varHttp.delete("hhttp://localhost/WeApiNew/api/Add/DeleteTaskDetails?taskID="+ value);
  //return this.varHttp.delete<Task>("http://localhost:52924/api/Add/DeleteTaskDetails?taskID="+value,httpOptions);

  // return this.varHttp.delete("http://localhost:52924/api/Add/DeleteTaskDetails"+value , httpOptions);

 }

 GetAllTaskDetails() : Observable<Task[]>
 { 
   return this.varHttp.get<Task[]>("http://localhost/WeApiNew/api/Add/GetAllTaskDetails");
}

}

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'    
  })
};
