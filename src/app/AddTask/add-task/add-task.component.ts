import { Component, OnInit } from '@angular/core';
import { AddTaskService } from 'src/app/Services/add-task.service';
import { Task } from 'src/app/Model/Task';
import { FormGroup,FormControl } from '@angular/forms';
import { Console } from '@angular/core/src/console';

@Component({
  selector: 'app-AddTask-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.css']
})
export class AddTaskComponent implements OnInit 
{
  frmTaskGroup = new FormGroup({
    //TaskID : new FormControl(''),
    TaskName :  new FormControl(''),
    Priority :new FormControl(''),
    Parent_ID : new FormControl(''),
    StartDate :new FormControl(''),
    EndDate : new FormControl('')

  });  

    public searchTask : string;
    public taskCollection : Task[];
    showMsg: string = null;
  constructor(private serviceVariable:AddTaskService) 
  { }

  ngOnInit() {
   // this.showTask();
    this.GetAllTaskDetails();
    //this.showTaskName();
  }
  showTask() {
    this.serviceVariable.getTask().subscribe((TaskIDVar:Task)=> this.frmTaskGroup.setValue(TaskIDVar));
   }
   showTaskName(value:any) {
     
    //this.serviceVariable.getTaskName(this.searchTask).subscribe((TaskIDVar:Task)=> this.frmTaskGroup.patchValue({TaskName : TaskIDVar.TaskName}));   //TaskIDVar.TaskName));
    this.serviceVariable.getTaskName(value.value).subscribe((TaskIDVar:Task)=> this.frmTaskGroup.patchValue({TaskName : TaskIDVar.TaskName}));    //TaskIDVar.TaskName));
 
   }
   AddTaskDetails()
   {  
     
     this.serviceVariable.AddTaskDetails(this.frmTaskGroup.value).subscribe(data=> {console.log('Success') });
     //Console.log("Success");
     this.showMsg = "Added Successfully";
    // this.showMsg = true;
  
   }

   UpdateTaskDetails()
   {  
     this.serviceVariable.ModifyTaskDetails(this.frmTaskGroup.value).subscribe();
   }

   DeleteTaskDetails(value:any)
   {
      
  //  this.serviceVariable.DeleteTaskDetails(value.value).subscribe((TaskIDVar:Task)=> this.frmTaskGroup.patchValue({TaskID : TaskIDVar.TaskID})); 
   //this.serviceVariable.DeleteTaskDetails(this.frmTaskGroup.value).subscribe();
    this.serviceVariable.DeleteTaskDetails(value.value).subscribe();
   }

   GetAllTaskDetails()
   {
     this.serviceVariable.GetAllTaskDetails().subscribe((TaskIDVar:Task[])=> this.taskCollection=TaskIDVar);

    // this.taskCollection = this.serviceVariable.GetAllTaskDetails().subscribe();

      // this.taskCollection = [new Task(1,'BHF11'),new Task()]

    //working fine this.taskCollection = [{"TaskID":1,"TaskName":"BHF1"},{"TaskID":2,"TaskName":"BHF2"},{"TaskID":3,"TaskName":"BHF3"}];
   }
   resetTaskForm(){
    this.frmTaskGroup.reset();
  }

  }
