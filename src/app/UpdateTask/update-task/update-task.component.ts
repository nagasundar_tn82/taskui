import { Component, OnInit } from '@angular/core'; 
import {ActivatedRoute} from '@angular/router';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import {FormGroup, FormControl} from '@angular/forms';
import {Validators} from '@angular/forms';
import { AddTaskService } from 'src/app/Services/add-task.service';
import {DatePipe} from '@angular/common';
import { Task } from 'src/app/Model/Task';


@Component({
  selector: 'app-update-task',
  templateUrl: './update-task.component.html',
  styleUrls: ['./update-task.component.css'],
  providers:[DatePipe]
})
export class UpdateTaskComponent implements OnInit 
{
  frmTaskGroup = new FormGroup({
    TaskID : new FormControl(''),
    Parent_ID : new FormControl(''),
    TaskName :  new FormControl('',Validators.required),
    StartDate : new FormControl(''),
    EndDate :  new FormControl(''),
    Priority :  new FormControl('',Validators.required),
    IsTaskEnd : new FormControl('')
  })

  public searchTask : string;
 // public taskCollection : Task[];
  constructor(private serviceVariable :AddTaskService, private route : ActivatedRoute,private datePipe : DatePipe) { }
  showMsg: boolean = false;
  showTaskID :boolean = false;
  ngOnInit() {
  
    this.loadTask()
  }
 loadTask(){
   if(this.route.snapshot.paramMap.get('TaskID')){
    //debugger;
    this.serviceVariable.getTaskName(this.route.snapshot.paramMap.get('TaskID')).subscribe(data=> this.frmTaskGroup.setValue(data));
    // debugger;
    // this.frmTaskGroup.patchValue({StartDate : "26/05/2019"});  
    
    ////workign finethis.serviceVariable.getTaskName(this.route.snapshot.paramMap.get('TaskID')).subscribe((TaskIDVar:Task)=> this.frmTaskGroup.patchValue({TaskName : TaskIDVar.TaskName}));    
    //this.serviceVariable.getTask().subscribe((TaskIDVar:Task)=> this.frmTaskGroup.setValue(TaskIDVar));
   // this.serviceVariable.getTaskName(value.value).subscribe((TaskIDVar:Task)=> this.frmTaskGroup.setValue(data));
   }
   else{
     console.log("Task ID not found");
   }
 }

 UpdateTaskDetails()
 {  
   this.serviceVariable.ModifyTaskDetails(this.frmTaskGroup.value).subscribe();   
   //this.showMsg = "Updated Successfully";
   this.showMsg = true;
 }
resetTaskForm(){
  this.frmTaskGroup.reset();
}
}
