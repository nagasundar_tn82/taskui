import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import { FormGroup,FormControl,FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { Task } from 'src/app/Model/Task';
import {HttpClientModule } from '@angular/common/http';
import { UpdateTaskComponent } from './update-task.component';
import {Observable} from 'rxjs';

describe('UpdateTaskComponent', () => {
  let component: UpdateTaskComponent;
  let fixture: ComponentFixture<UpdateTaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateTaskComponent ],
      imports: [ BrowserModule,       
        FormsModule,
        HttpClientModule,
        ReactiveFormsModule,
        RouterModule
        ,RouterTestingModule
      ],
      providers: [
        {provide:   UpdateTaskComponent }]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('Created for Update Task', () => {
    expect(component).toBeTruthy();
  });
});
