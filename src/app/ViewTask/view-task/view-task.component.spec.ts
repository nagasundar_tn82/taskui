import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { async, ComponentFixture, TestBed,inject } from '@angular/core/testing';
import { Task } from 'src/app/Model/Task';
import { ViewTaskComponent } from './view-task.component';
import {Observable, of,from} from 'rxjs';
import { AddTaskService } from 'src/app/Services/add-task.service';
import { FormGroup,FormControl,FormsModule } from '@angular/forms';
import { Pipe, PipeTransform } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { forEach } from '@angular/router/src/utils/collection';
import {SearchPipePipe} from 'src/app/Filters/search-pipe.pipe';
import {HttpClientModule} from '@angular/common/http';

import { Injectable } from '@angular/core';

import { HttpHeaders } from '@angular/common/http';
//import { expect } from 'jasmine';


//import 'rxjs/add/observable/fromEvent';

// import 'rxjs-compat/add/observable/from';
//import { describe } from 'jasmine';Y
//import rxjs/add/Observable/from;



describe('ViewTaskComponent', () => {
  let component: ViewTaskComponent;
  let fixture: ComponentFixture<ViewTaskComponent>;
  let service1: AddTaskService;
  let varHttp:HttpClient

  const viewAllTasks : Task[] = [
    {
     TaskID:1,
     TaskName:'Development',
     Parent_ID :1,
     Priority :4,
     StartDate :'10/05/2019',
     EndDate :'12/05/2019',
     IsTaskEnd : true
  },
  {
    TaskID:2,
    TaskName:'Testing',
    Parent_ID :21,
    Priority :5,
    StartDate :'15/05/2019',
    EndDate :'19/05/2019',
    IsTaskEnd : true
 }
];

 beforeEach(()=>{

  service1 = new AddTaskService(varHttp);
  component = new ViewTaskComponent(service1);
 });

  beforeEach(async(() => {
    TestBed.configureTestingModule({ 
      declarations: [ ViewTaskComponent,SearchPipePipe  ],
      
      imports: [ BrowserModule,       
        FormsModule,
        ReactiveFormsModule
        ,RouterModule
        ,HttpClientModule
      ],
      providers: [
         AddTaskService ]
    }).compileComponents();

    //     fixture = TestBed.createComponent(ViewTaskComponent);
    // //component = fixture.componentInstance;
    // fixture.detectChanges();


  }));

  it('Created to check View Task Component', () => {
    expect(component).toBeTruthy();
  });

  it('GetAllTaskDetails() should return not null tasks', inject([AddTaskService], (service: AddTaskService) => {
    //  service.getTasks().subscribe(value=> {expect(value.length).toBeGreaterThanOrEqual(1)});
    expect(service.GetAllTaskDetails()).not.toBeNull();
  }));


  // it('#getValue should return real value', () => {
  //   expect(service1.GetAllTaskDetails()).toBeTruthy();
  // });


  //  it("first",async(()=>{
  //    let value:boolean = false;
  //    expect(component).toBeTruthy();
  //  }));
  // beforeEach(() => {
  //   fixture = TestBed.createComponent(ViewTaskComponent);
  //   component = fixture.componentInstance;
  //   fixture.detectChanges();
  //   service = TestBed.get(AddTaskService);
  // });

  // it('should view all the tasks list', () => {    
  //   spyOn(service1,'GetAllTaskDetails').and.returnValue(of(ViewAllTasks));
  //   component.ngOnInit();
  // });
  // // it('should view all the tasks list', () => {    
  // //   spyOn(service1,'GetAllTaskDetails').and.callFake(()=>{return Observable.from([viewAllTasks])});
  // //   ///spyOn(component, 'changeTitle').and.callThrough();
  // //   component.ngOnInit();
  // // });

});
 
