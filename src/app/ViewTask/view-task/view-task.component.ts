import { Component, OnInit } from '@angular/core';
import { TaskService } from 'src/app/Services/TaskService';
import { TaskModel } from 'src/app/Models/TaskModel';
import { FormGroup,FormControl } from '@angular/forms';
import {Observable} from 'rxjs';
import { Data } from '@angular/router/src/config';
import { NgModule } from '@angular/core';
import { forEach } from '@angular/router/src/utils/collection';

@Component({
  selector: 'app-view-task',
  templateUrl: './view-task.component.html',
  styleUrls: ['./view-task.component.css']
})
export class ViewTaskComponent implements OnInit {



  // frmTaskGroup = new FormGroup({
  //   TaskID : new FormControl(''),
  //   TaskName :  new FormControl(''),
  //   Priority :new FormControl(''),
  //   Parent_ID : new FormControl(''),
  //   StartDate :new FormControl(''),
  //   EndDate : new FormControl('')

  // });  ")"
  public txtTask = new FormControl('');
  public txtParentTask = new FormControl('');
  public txtPrioriyFrom = new FormControl('');
  public txtPrioriyTo = new FormControl('');
  public dtStartDate = new FormControl('');
  public dtEndDate = new FormControl('');

 
 


  public eachTask : Task[];
  constructor(private serviceVariable:AddTaskService) {}
  public taskCollection : Task[];
  ngOnInit() {
     
    this.GetAllTaskDetails();
  }

  public GetAllTaskDetails()
  {
    this.serviceVariable.GetAllTaskDetails().subscribe((TaskIDVar:Task[])=> this.taskCollection=TaskIDVar);

   // this.taskCollection = this.serviceVariable.GetAllTaskDetails().subscribe();

     // this.taskCollection = [new Task(1,'BHF11'),new Task()]

   //working fine this.taskCollection = [{"TaskID":1,"TaskName":"BHF1"},{"TaskID":2,"TaskName":"BHF2"},{"TaskID":3,"TaskName":"BHF3"}];
  }

  EndTask(Task_ID : number)
  {
    let index =  this.taskCollection.findIndex(a=>a.TaskID == Task_ID);
    let TaskToUpdate = this.taskCollection[index];
    TaskToUpdate.IsTaskEnd = true;
    this.serviceVariable.ModifyTaskDetails(TaskToUpdate).subscribe();   
   //this.showMsg = "Updated Successfully";
  }
  // deleteTask(id:string){
  //   this.serviceVariable.deleteTask(id).subscribe(data=> {
      
  //    if(data){
  //      this.allTask.splice(this.allTask.findIndex(a=>a.Task_ID == id),1);
  //    }
  //    else{
  //      alert("Task is not deleted");
  //    }
  //    });
  //  }


  
  // endTask(id:any){
  //   let taskToEnd : Task;q 
  //   if(this.eachTask && this.eachTask. > 0)
  //   {
  //     taskToEnd = this.eachTask.find(a=>a.Task_ID == id);
  //     if(taskToEnd){
  //       this.taskService.updateTask(taskToEnd).subscribe(data=> {
  //         if(data){
  //           alert("Task updated");
  //         }
  //         else{
  //           alert("Task is not updated");
  //         }
  //       });
  //     }
  //   }
  // }


}
