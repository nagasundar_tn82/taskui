import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AddTaskComponent } from './AddTask/add-task/add-task.component';
import { ReactiveFormsModule } from '@angular/forms';
import {HttpClientModule } from '@angular/common/http';
import { AddTaskService } from 'src/app/Services/add-task.service';
import { SearchPipePipe } from './Filters/search-pipe.pipe';
import { UpdateTaskComponent } from './UpdateTask/update-task/update-task.component';
import { ViewTaskComponent } from './ViewTask/view-task/view-task.component';
import {DatePipe} from '@angular/common';
import {FormsModule} from  '@angular/forms';
 

 
import {RouterModule, Routes} from '@angular/router';
 
import { Injectable } from '@angular/core';

 
const appRoutes: Routes = [
  {path : 'AddTask', component : AddTaskComponent},
  {path : 'UpdateTask/:TaskID', component : UpdateTaskComponent},
  {path : 'ViewTask', component : ViewTaskComponent},
  {path : '', component : AddTaskComponent},
  ];

@NgModule({
  declarations: [
    AppComponent,    
    AddTaskComponent, 
    SearchPipePipe, UpdateTaskComponent, ViewTaskComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
    ,ReactiveFormsModule
    ,HttpClientModule
    ,FormsModule
    ,RouterModule.forRoot(appRoutes, {enableTracing : true})
  ],
  providers: [AddTaskService],
  bootstrap: [AppComponent]
})
export class AppModule { }
