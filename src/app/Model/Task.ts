 


export class Task { 
    public TaskID:number;
    public TaskName:string;
    public Parent_ID :number;
    public Priority :number;
    public StartDate :string;
    public EndDate :string;
    public IsTaskEnd : boolean;
}