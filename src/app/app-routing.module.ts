import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddTaskComponent } from 'src/app/AddTask/add-task/add-task.component'
import { UpdateTaskComponent } from 'src/app/UpdateTask/update-task/update-task.component';
import { ViewTaskComponent } from 'src/app/ViewTask/view-task/view-task.component';



// const routes: Routes = [{path:'addTask',component:AddTaskComponent}];


const routes: Routes = [
  // {path:'addTask',component:AddTaskComponent} 

{ path: 'AddTask', component: AddTaskComponent },
{ path: 'UpdateTask',        component: UpdateTaskComponent },
{ path: 'ViewTask',        component: ViewTaskComponent },

{ path: '',   redirectTo: '/AddTask', pathMatch: 'full' }];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
